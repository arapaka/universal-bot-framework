// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('./model');

// CONTROLLERS
// ---------------------
var util = require("./util");
var controller = require("./controller");

// CONST
// ---------------------
var constant = require("./constant");

/**
 * @api {post} /v1/test test
 * @apiVersion 1.0.0
 * @apiName test
 * @apiGroup Test
 * @apiPermission none
 *
 * @apiDescription just a test endpoint
 *
 * @apiSuccess {String} test
 */
exports.test = function test() {
    return function (req, res) {
        return util.Response.sendOK(req, res, {test: "successful"});
    }
};
