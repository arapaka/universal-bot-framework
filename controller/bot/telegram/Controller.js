// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Telegram"; // case-sensitive, must be the same as in config.bot.json and bot/index.js
var _bot = require("./Listener"),
    _maxButtons = 3,
    _maxElements = 30;

/**
 * sends a welcome message to the user
 * @param chatBotUserSession
 */
exports.sendWelcomeMessage = function sendWelcomeMessage(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.WELCOME.replace(":first_name:", chatBotUserSession.profile.first_name);
    exports.sendText(chatBotUserSession, text);
    exports.sendCommandChooser(chatBotUserSession);
};

/**
 * send command chooser
 * @param chatBotUserSession
 */
exports.sendCommandChooser = function sendCommandChooser(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.WHAT_WOULD_YOU_LIKE_TO_DO;
    var markup = _bot.keyboard([
        [constant.BOT.MESSAGE.START_VOTING],
        [constant.BOT.MESSAGE.CHOOSE_A_CATEGORY],
        [constant.BOT.MESSAGE.TRENDING_TAGS],
        [constant.BOT.MESSAGE.SEND_LINK]
    ], {resize: true});

    _bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup});
};

/**
 * send category chooser
 * @param chatBotUserSession
 * @param categories
 */
exports.sendCategoryChooserWithScrollView = function sendCategoryChooserWithScrollView(chatBotUserSession, categories) {
    var text = constant.BOT.MESSAGE.SELECT_A_CATEGORY,
        elements = [],
        counter = 0,
        emoji;

    // back to menu
    elements.push([constant.BOT.MESSAGE.BACK_TO_MENU]);

    var rowElements = [];

    for (var i = 0; i < categories.length; i++) {
        rowElements = [];
        while (rowElements.length < 2 && i < categories.length) {
            if (categories[i] && (categories[i].slug == "all" || categories[i].show.exploring && categories[i].show.creating) &&
                counter < _maxElements
            ) {
                emoji = (constant.BOT.CATEGORY_EMOJI[categories[i].slug] ? constant.BOT.CATEGORY_EMOJI[categories[i].slug] + " " : "");
                rowElements.push(emoji + " " + categories[i].name);
                counter++;
            }
            if (rowElements.length < 2) i++;
        }
        if (rowElements.length == 1) {
            rowElements.push(" ");
        }
        elements.push(rowElements);
    }

    var markup = _bot.keyboard(elements, {resize: true});
    _bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup})
        .catch(function (error) {
            console.error("sendCategoryChooserWithScrollView:sendMessage", error);
        });
};

/**
 * send dvel to vote
 * @param chatBotUserSession
 * @param dvel
 */
exports.sendDvelToVote = function sendDvelToVote(chatBotUserSession, dvel) {

    _bot.sendPhoto(chatBotUserSession.chatBotUserId, constant.BOT.MESSAGE.SWELL_SHARING_IMAGE_URL_PREFIX + (global.bots.config.production ? dvel.shortId : "ryXP28Gz"),
        {
            caption: dvel.user.name + ": " + (dvel.question || "#whatsBetter"),
            fileName: (global.bots.config.production ? dvel.shortId : "ryXP28Gz") + ".png",
            reply_markup: _bot.inlineKeyboard([
                [
                    _bot.inlineButton(constant.BOT.MESSAGE.VOTE_A_EMOJI, {
                        callback: JSON.stringify({action: constant.BOT.MESSAGE.VOTE_A})
                        // cant use payload here, because max size = 64 bytes
                        //JSON.stringify({
                        //    action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
                        //    dvelId: dvel.id,
                        //    photoId: dvel.photos[0].id
                        //})
                    }),
                    _bot.inlineButton(constant.BOT.MESSAGE.SKIP_EMOJI, {
                        callback: JSON.stringify({action: constant.BOT.MESSAGE.SKIP})
                        // cant use payload here, because max size = 64 bytes
                        //JSON.stringify({
                        //action: constant.BOT.MESSAGE.DO_SKIP_ACTION,
                        //dvelId: dvel.id
                        //})
                    }),
                    _bot.inlineButton(constant.BOT.MESSAGE.VOTE_B_EMOJI, {
                        callback: JSON.stringify({action: constant.BOT.MESSAGE.VOTE_B})
                        // cant use payload here, because max size = 64 bytes
                        //JSON.stringify({
                        //    action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
                        //    dvelId: dvel.id,
                        //    photoId: dvel.photos[1].id
                        //})
                    })
                ], [
                    _bot.inlineButton(constant.BOT.MESSAGE.OPEN_IN_APP_EMOJI, {
                        url: "http://swell.wtf/d/" + dvel.shortId
                    })
                ]
            ]),
            parse_mode: "markdown"
        })
        .then(function (response) {
            chatBotUserSession.settings.lastMessage = {
                chatId: chatBotUserSession.chatBotUserId,
                messageId: response.result.message_id
            };
            chatBotUserSession.save(function (error, result) {
                if (error) console.error("sendDvelToVote.sendPhoto.settings.lastMessage save", error);
            });
        })
        .catch(function (error) {
            console.error("sendDvelToVote:sendPhoto", error);
        });
};

/**
 * this method gets called from Bot.Controller
 * it returns a generic user profile with the same keys for every chat bot platform
 * @param chatBotUserId
 * @param callback function(profile), profile could be null if not found
 */
exports.getUserProfileGeneric = function getUserProfileGeneric(chatBotUserId, callback) {
    console.log("getUserProfileGeneric", chatBotUserId);
    _bot.getChat(chatBotUserId).then(function (result) {
        console.log("getUserProfileGeneric:getChat", result);
        if (result.result) {
            __getProfilePic(chatBotUserId, function (error, profilePicUrl) {
                callback({
                    id: chatBotUserId,
                    first_name: result.result.first_name,
                    last_name: result.result.last_name,
                    profile_pic: profilePicUrl,
                    locale: null,
                    timezone: null,
                    gender: result.result.gender ? result.result.gender == "male" ? 0 : 1 : 99
                });
            });
        } else {
            callback(null);
        }
    }).catch(function (error) {
        console.error("getUserProfileGeneric:getChat", error);
        callback(null);
    });
};

/**
 * fetch the url of the first profile picture (highest solution)
 * @param chatBotUserId
 * @param callback function(error, profilePicUrl)
 * @private
 */
function __getProfilePic(chatBotUserId, callback) {
    _bot.getUserPhoto(chatBotUserId).then(function (result) {
        if (result.result && result.result.total_count > 0) {
            var photos = result.result.photos[0], // first profile pic we found
                maxSize = 0,
                fileId;
            photos.forEach(function (photo) {
                if (photo.file_size > maxSize) {
                    maxSize = photo.file_size;
                    fileId = photo.file_id;
                }
            });
            _bot.getFile(fileId).then(function (result) {
                //console.log("getFile", result);
                //console.log("url:", bot.fileLink + result.result.file_path); // https://api.telegram.org/file/bot<token>/<file_path>
                callback(null, _bot.fileLink + result.result.file_path);
            }).catch(function (error) {
                console.error("getFile", error);
                callback(error, null);
            })
        } else {
            callback("no profile pics", null);
        }
    }).catch(function (error) {
        console.error("getUserPhoto", error);
        callback(error, null);
    });
}

/**
 * send text
 * @param chatBotUserSession
 * @param text
 */
exports.sendText = function sendText(chatBotUserSession, text) {
    _bot.sendMessage(chatBotUserSession.chatBotUserId, text);
};

/**
 * send link
 * @param chatBotUserSession
 * @param link
 * @param text
 * @param picUrl
 */
exports.sendLink = function sendLink(chatBotUserSession, link, text, picUrl) {
    _bot.sendPhoto(chatBotUserSession.chatBotUserId, picUrl,
        {
            caption: "",
            //fileName: "ryXP28Gz.png",
            reply_markup: _bot.inlineKeyboard([
                [
                    _bot.inlineButton(text, {url: link})
                ]
            ])
        })
        .catch(function (error) {
            console.error("sendPhoto", error);
        });
};

/**
 * remove voting buttons
 * @param chatBotUserSession
 */
exports.removeButtons = function removeButtons(chatBotUserSession) {
    if (chatBotUserSession.settings.lastMessage && chatBotUserSession.settings.lastMessage.chatId && chatBotUserSession.settings.lastMessage.messageId) {
        exports.editMarkup(chatBotUserSession.settings.lastMessage.chatId, chatBotUserSession.settings.lastMessage.messageId);

        chatBotUserSession.settings.lastMessage = {};
        chatBotUserSession.save(function (error, result) {
            if (error) console.error("removeButtons.settings.lastMessage save", error);
        });
    }
};

/**
 * edit markup of a message
 * @param chatId
 * @param messageId
 * @param markup
 * @private
 */
exports.editMarkup = function editMarkup(chatId, messageId, markup) {
    if (!markup)
        markup = _bot.inlineKeyboard([]); // remove

    _bot.editMarkup({chatId: chatId, messageId: messageId}, markup)
        .then(function (result) {
            //console.log("__editMarkup", result);
        })
        .catch(function (error) {
            //console.error("__editMarkup", error);
        });
};

/**
 * ask for users location
 * @param chatBotUserSession
 */
exports.askForLocation = function askForLocation(chatBotUserSession) {
    chatBotUserSession.settings.specialAction = constant.BOT.SPECIAL_ACTION.ASK_FOR_LOCATION;
    chatBotUserSession.save(function (error, result) {
        if (error) console.error("askForLocation:chatBotUserSession.settings.specialAction save", error);
    });

    var text = constant.BOT.MESSAGE.ASK_FOR_LOCATION;
    var markup = _bot.keyboard([
        [_bot.button("location", constant.BOT.MESSAGE.YES_SEND_LOCATION)],
        [constant.BOT.MESSAGE.NO_LATER]
    ], {resize: true});

    _bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup});

    // TODO maybe send a link to a webpage which then calls the endpoint and the endpoint does a IP to location and adds it to the userSession
};

/**
 * gets called if location not found through address search
 * @param chatBotUserSession
 */
exports.locationNotFoundAskAgain = function locationNotFoundAskAgain(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.SRY_CANT_FIND_CITY;
    var markup = _bot.keyboard([
        [_bot.button("location", constant.BOT.MESSAGE.SEND_LOCATION)],
        [constant.BOT.MESSAGE.CANCEL]
    ], {resize: true});

    _bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup});
};

/**
 * sends trending tags to user
 * @param chatBotUserSession
 */
exports.sendTrendingTags = function sendTrendingTags(chatBotUserSession) {
    var trendingTags = controller.Bot.Handler.getTrendingTags(_maxElements);
    var text = constant.BOT.MESSAGE.TRENDING_TAGS + ":",
        elements = [],
        counter = 0,
        emoji;

    // back to menu
    elements.push([constant.BOT.MESSAGE.BACK_TO_MENU]);

    var rowElements = [];

    for (var i = 0; i < trendingTags.length; i++) {
        rowElements = [];
        while (rowElements.length < 2 && i < trendingTags.length) {
            if (trendingTags[i] && counter < _maxElements) {
                rowElements.push(trendingTags[i]);
                counter++;
            }
            if (rowElements.length < 2) i++;
        }
        if (rowElements.length == 1) {
            rowElements.push(" ");
        }
        elements.push(rowElements);
    }

    var markup = _bot.keyboard(elements, {resize: true});
    _bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup})
        .catch(function (error) {
            console.error("sendCategoryChooserWithScrollView:sendMessage", error);
        });
};