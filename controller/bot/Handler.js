// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('./../../model');

// CONTROLLERS
// ---------------------
var util = require("./../../util");
var controller = require("./../../controller");

// CONST
// ---------------------
var constant = require("./../../constant");
var _categories = {};
var _trendingTags = {};

/**
 * init all data
 * refresh it every x min
 * @private
 */
function __initBotHandler() {
    // demo data
    _categories = [
        {
            slug: "swell",
            name: "Swell",
            icons: {large: "http://www.pholly.at/ph/wp-content/uploads/2016/07/iTunesArtwork@2x.png"}
        },
        {
            slug: "intracked",
            name: "Intracked",
            icons: {large: "http://www.pholly.at/ph/wp-content/uploads/2015/03/playstore-icon1.png"}
        },
        {
            slug: "almtour",
            name: "Almtour",
            icons: {large: "http://www.pholly.at/ph/wp-content/uploads/2015/03/playstore-icon.png"}
        },
        {
            slug: "johannesweg",
            name: "Johannesweg",
            icons: {large: "http://www.pholly.at/ph/wp-content/uploads/2014/06/icon_512px.png"}
        }
    ];

    _trendingTags = ["#whattobuy", "#whattoeat", "#whattodo", "#wheretogo", "#bestphoto", "#coolstuff"];

    setTimeout(__initBotHandler, 1000 * 60 * 2); // refresh data every x min
}
__initBotHandler();

exports.getCategories = function getCategories() {
    return _categories;
};
exports.getCategoryWithSlug = function getCategoryWithSlug(slug) {
    return util.Helpers.getObjectByIdFromArray(_categories, slug, "slug");
};
exports.getTrendingTags = function getTrendingTags(limit) {
    var tt = [].concat(_trendingTags); // copy it by value (not reference)
    return tt.splice(0, limit);
};

var userSessions = {}; // ChatBotUserSession.js
var delayAfterSetLocation = 2000;

exports.processMessage = function processMessage(chatBotPlatform, chatBotUserId, payload, req, callback) {
    if (typeof payload === "string") {
        console.warn("__processMessage: payload is string, convert it to object");
        payload = {action: payload, message: true};
    }

    // define req object
    if (!req)           req = {};
    if (!req.headers)   req.headers = {};
    if (!req.body)      req.body = {};

    // session
    __startUserSessionIfNeeded(chatBotPlatform, chatBotUserId, function (chatBotUserSession) {
        //console.log("__startUserSessionIfNeeded: userSession", chatBotUserSession);

        __setStartingMetaData(chatBotUserSession, payload, req);
        __resetUserStatesIfNeeded(chatBotUserSession, payload);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Set Location from object
        if (payload.action == constant.BOT.MESSAGE.LOCATION_SENT && payload.location) {
            __setLocation(chatBotUserSession, payload.location);
            setTimeout(function () {
                __checkIncomingMessage(chatBotUserSession, payload, req, callback);
            }, delayAfterSetLocation);
        } else {
            __checkIncomingMessage(chatBotUserSession, payload, req, callback);
        }
    });
};

/**
 * check what is in the payload and act according to it
 * @param chatBotUserSession
 * @param payload
 * @param req
 * @param callback
 * @private
 */
function __checkIncomingMessage(chatBotUserSession, payload, req, callback) {
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Set Location by given location name
    if (chatBotUserSession.settings.specialAction == constant.BOT.SPECIAL_ACTION.ASK_FOR_LOCATION) {
        console.log("BOT.SPECIAL_ACTION.ASK_FOR_LOCATION");
        util.Location.getLocationOfAddress(payload.action, function (location) {
            if (location) { // success
                __setLocation(chatBotUserSession, location);
                setTimeout(function () {
                    __checkIncomingMessage(chatBotUserSession, payload, req, callback);
                    //exports.processMessage(chatBotUserSession.chatBotPlatform, chatBotUserSession.chatBotUserId, payload, req, callback);
                }, delayAfterSetLocation);
            } else {
                controller.Bot[chatBotUserSession.chatBotPlatform].Controller.locationNotFoundAskAgain(chatBotUserSession);
            }
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // START
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.START
            ]
        )
    ) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendWelcomeMessage(chatBotUserSession);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LOCATION
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.LOCATION
            ]
        )
    ) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.askForLocation(chatBotUserSession);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CHOOSE A CATEGORY
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.CHOOSE_A_CATEGORY
            ]
        )
    ) {
        if (!__checkToThrowSpecialAction(chatBotUserSession, payload)) {
            //console.warn("action not thrown");
            controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendCategoryChooserWithScrollView(chatBotUserSession, _categories);
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CHANGE THE CATEGORY WITH GIVEN SLUG
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.CHANGE_CATEGORY_ACTION
            ]
        ) || (payload.categorySlug = helpers.getCategorySlugOfString(payload.action)) // is there a category in text?
    ) {
        if (payload.categorySlug) {
            // ...
        }
        // voting
        var dvel = {
            id: "ryXP28Gz",
            shortId: "ryXP28Gz",
            user: {name: "Phil"},
            question: "#whatsbetter",
            photos: [{id: "1"}, {id: "2"}]
        };
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendDvelToVote(chatBotUserSession, dvel);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // START VOTING
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.START_VOTING
            ]
        )
    ) {
        // ...
        // voting
        if (!__checkToThrowSpecialAction(chatBotUserSession, payload)) {
            //console.warn("action not thrown");
            var dvel = {
                id: "ryXP28Gz",
                shortId: "ryXP28Gz",
                user: {name: "Phil"},
                question: "#whatsbetter",
                photos: [{id: "1"}, {id: "2"}]
            };
            controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendDvelToVote(chatBotUserSession, dvel);
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // START VOTING WITH TAG
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action, _trendingTags)) {
        // ...
        // voting
        if (!__checkToThrowSpecialAction(chatBotUserSession, payload)) {
            //console.warn("action not thrown");
            var dvel = {
                id: "ryXP28Gz",
                shortId: "ryXP28Gz",
                user: {name: "Phil"},
                question: "#whatsbetter",
                photos: [{id: "1"}, {id: "2"}]
            };
            controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendDvelToVote(chatBotUserSession, dvel);
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DOWNLOAD APP
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.SEND_LINK
            ]
        )
    ) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendLink(
            chatBotUserSession,
            constant.BOT.MESSAGE.WEB_URL,
            constant.BOT.MESSAGE.GO_TO_LINK,
            constant.BOT.MESSAGE.PHOLLY_PIC_HEADER
        );
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // OPEN IN APP
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action,
            [
                constant.BOT.MESSAGE.OPEN_IN_APP,
                constant.BOT.MESSAGE.OPEN_IN_APP_EMOJI
            ]
        )
    ) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendLink(
            chatBotUserSession,
            "http://swell.wtf/d/ryXP28Gz",
            constant.BOT.MESSAGE.OPEN_IN_APP,
            constant.BOT.MESSAGE.SWELL_SHARING_IMAGE_URL_PREFIX + "ryXP28Gz"
        );
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ANY CANCEL MESSAGE
    else if (helpers.isStringInHaystackInCaseSensitive(payload.action, __getCancelMessages())
    ) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendCommandChooser(chatBotUserSession);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // OTHERS
    else {
        if (!__checkToThrowSpecialAction(chatBotUserSession, payload)) {
            //console.warn("action not thrown");
            controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendCommandChooser(chatBotUserSession);
        }
    }

    // check if we have to remove the lastMessage, or remove inline buttons
    if (chatBotUserSession.settings.lastMessage && chatBotUserSession.settings.lastMessage.chatId && chatBotUserSession.settings.lastMessage.messageId) {
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.removeButtons(chatBotUserSession);
    }
}

/**
 * start a user session
 * @param chatBotPlatform
 * @param chatBotUserId
 * @param callback
 * @returns {*}
 * @private
 */
function __startUserSessionIfNeeded(chatBotPlatform, chatBotUserId, callback) {
    if (__getSession(chatBotPlatform, chatBotUserId)) {
        console.log("__startUserSessionIfNeeded:UserSession already exists in cache");
        return callback(__getSession(chatBotPlatform, chatBotUserId));
    }

    controller.Bot[chatBotPlatform].Controller.getUserProfileGeneric(chatBotUserId, function (profile) {
            if (!profile) {
                console.error("__startUserSessionIfNeeded:getUserProfileGeneric is empty", profile);
            }
            // now select the session from mongo
            model.mongo.ChatBotUserSession.findOne({
                    chatBotUserId: chatBotUserId,
                    chatBotPlatform: chatBotPlatform
                }, function (error, chatBotUserSession) {
                    if (error || !chatBotUserSession) {
                        console.error("__startUserSessionIfNeeded:ChatBotUserSession.findOne", error || "cant find", "-> creating");
                        // create session
                        __createChatBotUserSession(chatBotPlatform, chatBotUserId, profile, function (chatBotUserSession) {
                            // restart this function __startUserSessionIfNeeded
                            __startUserSessionIfNeeded(chatBotPlatform, chatBotUserId, callback);
                        });
                        return;
                    }
                    if (profile) { // update profile
                        chatBotUserSession.profile = profile;
                        chatBotUserSession.save(function (error, result) {
                            if (error) console.error("__startUserSessionIfNeeded:chatBotUserSession.profile save", error);
                        });
                    }

                    __setSession(chatBotPlatform, chatBotUserId, chatBotUserSession);
                    callback(chatBotUserSession);
                }
            );
        }
    );
}

/**
 * get session
 * @param chatBotPlatform
 * @param chatBotUserId
 * @returns {*}
 * @private
 */
function __getSession(chatBotPlatform, chatBotUserId) {
    return userSessions[chatBotPlatform + "_" + chatBotUserId];
}

/**
 * set session
 * @param chatBotPlatform
 * @param chatBotUserId
 * @param chatBotUserSession
 * @private
 */
function __setSession(chatBotPlatform, chatBotUserId, chatBotUserSession) {
    userSessions[chatBotPlatform + "_" + chatBotUserId] = chatBotUserSession;
}

/**
 * create a new user session in mongo
 * @param chatBotPlatform
 * @param chatBotUserId
 * @param profile
 * @param callback
 * @private
 */
function __createChatBotUserSession(chatBotPlatform, chatBotUserId, profile, callback) {
    model.mongo.ChatBotUserSession.create({
        chatBotPlatform: chatBotPlatform,
        chatBotUserId: chatBotUserId,
        profile: profile,
        chatHistory: [],
        settings: {}
    }, function (error, chatBotUserSession) {
        if (error) console.error("__createChatBotUserSession", error || "cant create ChatBotUserSession");
        console.log("__createChatBotUserSession created", chatBotUserSession);
        __setSession(chatBotPlatform, chatBotUserId, chatBotUserSession);
        callback(chatBotUserSession);
    });
}

/**
 * set history, user-agent and location
 * @param chatBotUserSession
 * @param payload
 * @param req
 * @private
 */
function __setStartingMetaData(chatBotUserSession, payload, req) {
    // set history, user-agent and location
    payload.time = new Date();
    chatBotUserSession.chatHistory.push(payload);
    req.headers['user-agent'] = "chatbot_" + chatBotUserSession.chatBotPlatform;
    chatBotUserSession.lastReq = req;
    chatBotUserSession.lastReq.body.lat = chatBotUserSession.settings.location.lat;
    chatBotUserSession.lastReq.body.long = chatBotUserSession.settings.location.long;
    chatBotUserSession.save(function (error, result) {
        if (error) console.error("chatBotUserSession.chatHistory save", error);
    });
}

/**
 * resets states of a user if needed
 * @param chatBotUserSession
 * @param payload
 * @private
 */
function __resetUserStatesIfNeeded(chatBotUserSession, payload) {
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // reset the specialAction if CANCEL, ... is sent
    if (chatBotUserSession.settings.specialAction == constant.BOT.SPECIAL_ACTION.ASK_FOR_LOCATION &&
        helpers.isStringInHaystackInCaseSensitive(payload.action, __getCancelMessages())
    ) {
        chatBotUserSession.settings.specialAction = null; // do not check it in the next step (if)
        chatBotUserSession.save(function (error, result) {
            if (error) console.error("chatBotUserSession.settings.location save", error);
        });
    }
}

/**
 * returns all cancel messages as an array
 * @returns {*[]}
 * @private
 */
function __getCancelMessages() {
    return [
        constant.BOT.MESSAGE.START,
        constant.BOT.MESSAGE.CANCEL,
        constant.BOT.MESSAGE.EXIT,
        constant.BOT.MESSAGE.END,
        constant.BOT.MESSAGE.STOP,
        constant.BOT.MESSAGE.NO_LATER,
        constant.BOT.MESSAGE.NO,
        constant.BOT.MESSAGE.COMMANDS,
        constant.BOT.MESSAGE.MENU,
        constant.BOT.MESSAGE.BACK_TO_MENU
    ];
}

/**
 * set location
 * @param chatBotUserSession
 * @param location object {lat, long}
 * @private
 */
function __setLocation(chatBotUserSession, location) {
    chatBotUserSession.settings.location = location;
    chatBotUserSession.settings.specialAction = null; // do not check it in the next step (if)
    chatBotUserSession.save(function (error, result) {
        if (error) console.error("chatBotUserSession.settings.location save", error);
    });
    controller.Bot[chatBotUserSession.chatBotPlatform].Controller.sendText(chatBotUserSession, constant.BOT.MESSAGE.THANKS_FOR_LOCATION);
}

/**
 * here you can throw special questions and actions
 * @param chatBotUserSession
 * @param payload
 * @return boolean thrown?
 * @private
 */
function __checkToThrowSpecialAction(chatBotUserSession, payload) {
    if (helpers.isStringInHaystackInCaseSensitive(payload.action, __getCancelMessages())) {
        console.log("__checkToThrowSpecialAction:", "there was an cancel word, do not throw");
        return false; // do not throw any if the
    }
    var interactionCount = chatBotUserSession.chatHistory.length;
    console.log("__checkToThrowSpecialAction:", "check for doing special action, interactionCount:", interactionCount);
    // LOCATION
    if (interactionCount > 0 && interactionCount % 20 == 0 && !chatBotUserSession.settings.location.lat) { // every Xth time
        console.log("__checkToThrowSpecialAction:", "askForLocation");
        controller.Bot[chatBotUserSession.chatBotPlatform].Controller.askForLocation(chatBotUserSession);
        return true;
    } else if (false) {
        // TODO other questions
    }

    return false; // no interaction
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////// HELPERS ////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @type {{compareStringsInCaseSensitive: helpers.compareStringsInCaseSensitive, isStringInHaystackInCaseSensitive: helpers.isStringInHaystackInCaseSensitive, getCategorySlugOfString: helpers.getCategorySlugOfString}}
 */
var helpers = {
    /**
     * string == String => true
     * StF == stf => true
     * @param string1 string
     * @param string2 string
     * @returns {boolean}
     */
    compareStringsInCaseSensitive: function compareStringsInCaseSensitive(string1, string2) {
        if (!string1 || !string2) return false; // undefined?
        return string1 == string2 || string1.toLowerCase() == string2.toLowerCase();
    },
    /**
     * checks if the given string is in the haystack
     * in case sensitive
     * @param string string
     * @param haystack array
     * @returns {boolean}
     */
    isStringInHaystackInCaseSensitive: function isStringInHaystackInCaseSensitive(string, haystack) {
        if (!haystack.length) {
            return false;
        }
        for (var i = 0; i < haystack.length; i++) {
            if (this.compareStringsInCaseSensitive(string, haystack[i])) {
                return true;
            }
        }
        return false;
    },
    /**
     * returns the slug of a category if the given string matches any
     * @param string string
     * @returns {bool|string}
     */
    getCategorySlugOfString: function getCategorySlugOfString(string) {
        for (var i in _categories) {
            if (_categories.hasOwnProperty(i)) {
                var name = _categories[i].name,
                    slug = _categories[i].slug;
                if (this.isStringInHaystackInCaseSensitive(string,
                        [
                            name,
                            slug
                        ]
                    )
                ) {
                    return slug;
                }
            }
        }
        return null;
    }
};