// BASIC FRAMEWORKS
// ---------------------
var FBBotFramework = require('./fb-bot-framework');

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Messenger"; // case-sensitive, must be the same as in config.bot.json and bot/index.js

// CONFIG
// ---------------------
var config = require("../../../config/config.bot")[global.bots.stageToUse][_chatBotPlatform];

exports.isEnabled = function isEnabled() {
    return !util.Helpers.isEmpty(config.page_token);
};
if (!exports.isEnabled()) {
    console.warn("Not initializing Facebook bot");
    return;
}

// Initialize Bot
var bot = new FBBotFramework({
    page_token: config.page_token,
    verify_token: config.verify_token
});

bot.setWelcomePayload(config.page_id, "{\"action\":\"start\"}", function (error, result) {
    if (error) {
        console.error("setWelcomePayload", error);
    }
});

// max 5 elements
bot.setPersistentMenu(config.page_id, [
    {
        "type": "postback",
        "title": constant.BOT.MESSAGE.START_VOTING,
        "payload": JSON.stringify({action: constant.BOT.MESSAGE.START_VOTING})
    },
    {
        "type": "postback",
        "title": constant.BOT.MESSAGE.CHOOSE_A_CATEGORY,
        "payload": JSON.stringify({action: constant.BOT.MESSAGE.CHOOSE_A_CATEGORY})
    },
    {
        "type": "postback",
        "title": constant.BOT.MESSAGE.TRENDING_TAGS,
        "payload": JSON.stringify({action: constant.BOT.MESSAGE.TRENDING_TAGS})
    },
    {
        "type": "web_url",
        "title": constant.BOT.MESSAGE.SEND_LINK,
        "url": constant.BOT.MESSAGE.WEB_URL
    }
], function (error, result) {
    if (error) {
        console.error("setPersistentMenu", error);
    }
});

// Setup listener for incoming messages
bot.on('message', function (chatBotUserId, message, req) {
    console.log('message:', chatBotUserId, message);
    __processMessage(chatBotUserId, {action: message, message: true}, req);
});

// Setup listener for postback
bot.on('postback', function (chatBotUserId, payload, req) {
    console.log('postback:', chatBotUserId, payload);
    __processMessage(chatBotUserId, JSON.parse(payload), req);
});

// Setup listener for image
bot.on('image', function (chatBotUserId, payload, req) {
    console.log('image:', chatBotUserId, payload);
    // payload: { url: 'https://scontent.xx.fbcdn.net/v/t35.0-12/13632759_10209791983738579_1540369439_o.jpg?_nc_ad=z-m&oh=86eb178e6facf59df9d96901bfac177c&oe=57818DE4' }
    __processMessage(chatBotUserId, {action: constant.BOT.MESSAGE.COMMANDS}, req);
});

// Setup listener for location
bot.on('location', function (chatBotUserId, payload, req) {
    console.log('location:', chatBotUserId, payload);
    // payload: { coordinates: { lat: 48.198195443518, long: 16.407001464173 } }
    __processMessage(chatBotUserId, {action: constant.BOT.MESSAGE.LOCATION_SENT, location: payload.coordinates}, req);
});

/**
 * this is the first method after the webhook is called
 * it starts all the rest, session, processing, ...
 * @param chatBotUserId
 * @param payload object {action: ""}
 * @param req
 * @private
 */
function __processMessage(chatBotUserId, payload, req) {
    console.log("__processMessage", chatBotUserId, payload);
    bot.sendSenderAction(chatBotUserId, "typing_on");
    controller.Bot.Handler.processMessage(
        _chatBotPlatform, // chatBotPlatform
        chatBotUserId, // chatBotUserId
        payload, // payload
        req, // req
        function (error, response) {
            console.log("processMessage:after", error, response);
        }
    );
}

bot.isEnabled = exports.isEnabled;
module.exports = bot;