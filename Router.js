var express = require('express');
var Router = express.Router();
var route = require('./route');
var util = require('./util');

// STATUS ROUTES
util.ApiStatus.injectApiStatusRoute(Router);

// ROUTES
Router.route('/' + global.bots.config.apiVersion + '/test').get(route.test());

module.exports = Router;